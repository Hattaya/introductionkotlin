package org.kotlinlang.play
fun someCondition() = true
fun main() {
    var a: String = "initial"
    println(a)
    val b: Int = 1
    val c = 3
    println(b)
    println(c)

    val d: Int
    if (someCondition()) {
        d = 1
    }else {
        d = 2
    }
    println("d = " + d)
}